//http module
//powerful building block to creating networking applications
//example we can create a server to listen to events on a port
//can create back end service for client apps ie react, anguler, mobile etc
const http = require('http');
const server = http.createServer((req, res) => {
    if (req.url === '/') {
        res.write('Hello world');
        res.end();
    }
    if (req.url === '/api/courses') {
        res.write(JSON.stringify([1, 2, 3]));
        res.end();
    }
});

//inherits from net.Server which is an event emitter
//Some events
//server.on
//server.addListener
//server.emit
// server.on('connection', (socket) => {
//     console.log('new connection');
// })
server.listen(3000);

console.log('Listening on port 3000');