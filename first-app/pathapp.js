//using build in modules
//the argument, node assumes it is a built in module
//then node looks for a relative path to a file in the application
const path = require('path');
var pathObj = path.parse(__filename);
//{ root: 'C:\\',
//dir: 'C:\\Users\\e\\Desktop\\workspace\\nodejs-tutorial\\first-app',
//base: 'app.js',
//ext: '.js',
//name: 'app' }
console.log(pathObj);