// Node wraps this in a function when it runs
//  
// (function (exports, require, module, __filename, __dirname) { var x = ; ... })
//immediately invoked function expression 'IIFE'
//node does not run our code directly
// __fileName is complete path to this module/file
// __dirname is path to directory that contains this module/file

//events
//camel case upper case indicates it is a class
//he defines class is like a human, the properties of all humans
const EventEmitter = require('events');

var url = 'http://mylogger.io/log';

class Logger extends EventEmitter {

    log(message) {
        //send an http request
        console.log(message);
        //Raise an event
        //emitter has many methods
        // - use emit to make a noise, produce - signalling event has happened
        //Raise event, need to also include a listener for the even is raised
        this.emit('messageLogged', { id: 1, url: 'http://' });
    }
}

// make function public to use elsewhere
// only export what is needed ie hide implementation details
// can use an object or a single function
// module.exports.log = log; or module.exports = log;
// or can also say exports.log = log; - this references the exports in the
// functio wrapper
//module.exports.log = log;
module.exports = Logger;
//module.exports.endPoint = url;