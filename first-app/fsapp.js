//working with files in node
const fs = require('fs');
//almost all methods come in asysn or sync, try no to use sync
//async is non blocking

//const filesSync = fs.readdirSync('./');
//console.log(filesSync);
//all of these methods have a second param that is a callback
fs.readdir('./', function(err, files) {
    if (err) {
        console.log('Error', err);
    } else {
        console.log('Result', files);
    }
});
//error
fs.readdir('$', function(err, files) {
    if (err) {
        console.log('Error', err);
    } else {
        console.log('Result', files);
    }
});