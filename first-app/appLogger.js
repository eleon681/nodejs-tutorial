//when we declare a var
// var message = '';
// are scoped not added to globals
// console.log(globals.message) does not work - undefined

//variables etc are scoped to that 'module'
//Creating a module

//loading modules
// ./ means same current dir
// load the exported object, 
// usually store in a const so we dont change the value
// this can be done with jshint
// set as const allows us to catch changes at compile time vs runtime
const logger = require('./logger');
console.log(logger);
//logger.log('passed message');
logger('passed message');