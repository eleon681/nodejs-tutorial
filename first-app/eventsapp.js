//events
//camel case upper case indicates it is a class
//he defines class is like a human, the properties of all humans
const EventEmitter = require('events');

//register a listener
//can also use emitter.addListener()
//we use emitter.on() more often
//must register before the event is called
// emitter.on('messageLogged', function(arg) {
//     console.log('Listener called', arg);
// });



// //Raise an event
// //emitter has many methods
// // - use emit to make a noise, produce - signalling event has happened
// //Raise event, need to also include a listener for the even is raised
// emitter.emit('messageLogged', { id: 1, url: 'http://' });

//Raise: Logging (data: message)
//send some data

const Logger = require('./logger');
const logger = new Logger();

//es6 version
logger.on('messageLogged', (arg) => {
    console.log('Listener called', arg);
});


logger.log('message');